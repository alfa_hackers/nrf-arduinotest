#include <Arduino.h>
#include <SPI.h>
#include <RF24.h>
#include <NeoPixelBus.h>

const uint16_t PixelCount = 1;
const uint8_t PixelPin = 6;

const uint64_t address = 21071992;

typedef struct Message {
	uint8_t index;
//	uint64_t  timeStamp;
	uint8_t red;
	uint8_t green;
	uint8_t blue;

	void printMessage() {
		Serial.print("number: ");
		Serial.println(this->index);
//		Serial.print("timeStamp: ");
//		Serial.println((long)this->timeStamp);
		Serial.println("\t\t ==Color==");
		Serial.print("\t\t Red: ");
		Serial.println(red);
		Serial.print("\t\t Green: ");
		Serial.println(green);
		Serial.print("\t\t Blue: ");
		Serial.println(blue);
	}
};

//int rgb[3] = {255,255,255};
RF24 radio(7, 8);
NeoPixelBus<NeoRgbFeature, Neo800KbpsMethod> pixel(PixelCount, PixelPin);

void setup() {
	Serial.begin(115200);
	pixel.Begin();
	pixel.Show();
	radio.begin();
	radio.openReadingPipe(1, address);
	radio.startListening();
}

void loop() {

	Message message;
	if (radio.available()) {
		while (radio.available()) {
			radio.read(&message, sizeof(message));
		}
		Serial.println("==Received==");
		message.printMessage();
		RgbColor color = RgbColor(message.red, message.green, message.blue);
		pixel.SetPixelColor(0, color);
		pixel.Show();
	}
}
